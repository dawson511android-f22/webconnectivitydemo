package ca.dawson511.webconnectivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import ca.dawson511.webconnectivity.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import androidx.lifecycle.lifecycleScope
import com.google.gson.JsonObject
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.launch

import javax.net.ssl.HttpsURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        handleThreading()
    }


    private fun getWebContent(): String {
        val url = URL("https://en.wikipedia.org/wiki/Android_(operating_system)");
        val conn : HttpsURLConnection = url.openConnection() as HttpsURLConnection

        conn.requestMethod = "GET"
        conn.connect()

        val response = conn.inputStream.reader().readText()

        conn.inputStream.close()
        conn.disconnect()

        return response
    }

    private fun getJSONContent(): String {

        val GSON: Gson = GsonBuilder().setPrettyPrinting().create()

        val url = URL("https://anime-facts-rest-api.herokuapp.com/api/v1/fma_brotherhood");
        val conn : HttpsURLConnection = url.openConnection() as HttpsURLConnection

        conn.requestMethod = "GET"
        conn.connect()

        val response = conn.inputStream.bufferedReader()
        val json : JsonObject = GSON.fromJson(response, JsonObject::class.java)

        return json.get("total_facts").asString
        //return json.toString()
    }

    private fun handleThreading(){
        val context = this
        lifecycleScope.launch(Dispatchers.IO) {
            //val data = getWebContent()
            val data = getJSONContent()

            withContext(Dispatchers.Main) {
                if (data != null) {
                    binding.textout.text = data
                } else {
                    Toast.makeText(context, "Could not find connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }


    }
}